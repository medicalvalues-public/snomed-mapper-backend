import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { map, Observable } from 'rxjs';

export interface Snomed {
  snomed: string;
  description: string;
}

interface SnomedResponse {
  items: Array<{
    term: string;
    concept: {
      conceptId: string;
      fsn: {
        term: string;
      };
    };
  }>;
}

export interface ISnowstormAdapterService {
  getSuggestionsDescriptionApi: (
    test: string,
    semanticTag: string,
  ) => Observable<Snomed[]>;
}

@Injectable()
export class SnowstormAdapterService implements ISnowstormAdapterService {
  private snowstormUrl = this.config.getOrThrow<string>('snowstorm.url');
  private snowstormBranch = this.config.getOrThrow<string>('snowstorm.branch');

  constructor(private config: ConfigService, private http: HttpService) {}

  getSuggestionsDescriptionApi(
    text: string,
    semanticTag: string | null = null,
  ): Observable<Snomed[]> {
    const params: Record<string, number | boolean | string> = {
      limit: 20,
      term: text,
      active: true,
      conceptActive: true,
      language: 'en',
      groupByConcept: true,
    };

    if (semanticTag) {
      params['semanticTag'] = semanticTag;
    }

    const url = `${this.snowstormUrl}/browser/${this.snowstormBranch}/descriptions`;
    return this.http.get<SnomedResponse>(url, { params }).pipe(
      map((response) =>
        response.data.items.map((item) => ({
          snomed: item.concept.conceptId,
          description: item.concept.fsn.term,
        })),
      ),
    );
  }
}
