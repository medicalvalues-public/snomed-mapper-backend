import { Test, TestingModule } from '@nestjs/testing';
import { SnowstormAdapterService } from './snowstorm-adapter.service';
import { HttpModule, HttpService } from '@nestjs/axios';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { firstValueFrom, of } from 'rxjs';
import SpyInstance = jest.SpyInstance;

describe('SnowstormAdapterService', () => {
  const testConfig = {
    snowstorm: {
      url: 'mySnowStorm',
      branch: 'myBranch',
    },
  };

  let service: SnowstormAdapterService;
  let http: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SnowstormAdapterService, ConfigService],
      imports: [
        ConfigModule.forRoot({
          load: [() => testConfig],
        }),
        HttpModule,
      ],
    }).compile();

    service = module.get<SnowstormAdapterService>(SnowstormAdapterService);
    http = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('descriptionsApi', () => {
    const mockSuggestions = {
      items: [
        {
          term: 'firstSuggestion',
          concept: {
            conceptId: '111',
            fsn: { term: 'firstSuggestion' },
          },
        },
        {
          term: 'secondSuggestion',
          concept: {
            conceptId: '222',
            fsn: { term: 'secondSuggestion' },
          },
        },
      ],
    };

    let spyOnGet: SpyInstance;
    let expectedEndpoint: string;

    beforeEach(() => {
      expectedEndpoint = `${testConfig.snowstorm.url}/browser/${testConfig.snowstorm.branch}/descriptions`;

      spyOnGet = jest.spyOn(http, 'get').mockReturnValueOnce(
        of({
          data: mockSuggestions,
          status: 200,
          statusText: 'okay',
          headers: {},
          config: {},
        }),
      );
    });

    it('calls description api ', async () => {
      const searchQuery = 'my search-by-text query';
      await firstValueFrom(service.getSuggestionsDescriptionApi(searchQuery));

      const expectedQueryParams = {
        active: true,
        conceptActive: true,
        groupByConcept: true,
        term: searchQuery,
      };

      expect(spyOnGet).toHaveBeenCalledWith(expectedEndpoint, {
        params: expect.objectContaining(expectedQueryParams),
      });
    });

    it('uses sematic Tag if provided', async () => {
      const searchQuery = 'my search-by-text query';
      const semanticTag = 'disorder';
      await firstValueFrom(
        service.getSuggestionsDescriptionApi(searchQuery, semanticTag),
      );

      const expectedQueryParams = {
        semanticTag,
      };

      expect(spyOnGet).toHaveBeenCalledWith(expectedEndpoint, {
        params: expect.objectContaining(expectedQueryParams),
      });
    });

    it('returns suggestions', async () => {
      const suggestions = await firstValueFrom(
        service.getSuggestionsDescriptionApi('i search-by-text for something'),
      );

      expect(suggestions).toEqual([
        { snomed: '111', description: 'firstSuggestion' },
        { snomed: '222', description: 'secondSuggestion' },
      ]);
    });
  });
});
