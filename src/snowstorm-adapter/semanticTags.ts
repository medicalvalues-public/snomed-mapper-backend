export const SemanticTags = [
  'disorder',
  'finding',
  'procedure',
  'substance',
  'medicinal product',
];
