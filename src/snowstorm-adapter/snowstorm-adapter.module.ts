import { Module } from '@nestjs/common';
import { SnowstormAdapterService } from './snowstorm-adapter.service';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';

@Module({
  providers: [SnowstormAdapterService],
  imports: [HttpModule, ConfigModule],
  exports: [SnowstormAdapterService],
})
export class SnowstormAdapterModule {}
