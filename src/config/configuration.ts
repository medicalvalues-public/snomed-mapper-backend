export default () => ({
  port: (process.env.PORT && parseInt(process.env.PORT, 10)) || 3000,
  database: {
    url: process.env.POSTGRES_URI,
  },
  snowstorm: {
    url: process.env.SNOWSTORM_URL,
    branch: process.env.SNOWSTORM_BRANCH,
  },
});
