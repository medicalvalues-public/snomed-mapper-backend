import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { UploadController } from './upload.controller';
import { diskStorage } from 'multer';
import { CsvImportService } from './csv-import/csv-import.service';

@Module({
  controllers: [UploadController],
  imports: [
    MulterModule.register({
      storage: diskStorage({}),
      limits: {
        fileSize: 1024 * 1024 * 10 /* 10 MB */,
      },
    }),
  ],
  providers: [CsvImportService],
})
export class UploadModule {}
