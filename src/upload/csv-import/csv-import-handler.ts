import { DataSource, QueryRunner, Repository } from 'typeorm';
import { Cell } from '../../data/entity/cell';
import { Column } from '../../data/entity/column';
import { Dataset } from '../../data/entity/dataset';
import { Row } from '../../data/entity/row';

export class CsvImportHandler {
  private queryRunner: QueryRunner;
  private rowRepository: Repository<Row>;
  private columnRepository: Repository<Column>;
  private cellRepository: Repository<Cell>;
  private initialized: Promise<void>;
  private rowIndex = 0;
  private colIndex = 0;
  private columns = new Map<string, Column>();
  private newColumns = new Array<Column>();
  private newRows = new Array<Row>();
  private newCells = new Array<Cell>();
  private writeOperations = new Array<Promise<unknown>>();

  constructor(private dataset: Dataset, dataSource: DataSource) {
    this.queryRunner = dataSource.createQueryRunner();
    this.initialized = this.initialize();
    this.writeOperations.push(this.initialized);
    this.rowRepository = this.queryRunner.manager.getRepository(Row);
    this.columnRepository = this.queryRunner.manager.getRepository(Column);
    this.cellRepository = this.queryRunner.manager.getRepository(Cell);
  }

  public async addRow(rowData: Record<string, string>): Promise<void> {
    await this.initialized;

    const row = new Row();
    row.dataset = this.dataset;
    row.rowIndex = this.rowIndex++;
    this.newRows.push(row);

    for (const [columnName, value] of Object.entries(rowData)) {
      this.addCell(row, columnName, value);
    }

    return this.writeBuffer();
  }

  public async finalize(): Promise<Dataset> {
    await Promise.all(this.writeOperations);
    await this.flushBuffer();
    await this.queryRunner.commitTransaction();
    this.dataset.rowCount = this.rowIndex;
    return this.dataset.save();
  }

  private async initialize(): Promise<void> {
    await this.queryRunner.startTransaction();
    await this.queryRunner.manager.save(this.dataset);
  }

  private async writeBuffer(): Promise<void> {
    if (this.newCells.length > 500) {
      const promise = this.flushBuffer();
      this.writeOperations.push(promise);
      await promise;
    }
  }

  private async flushBuffer() {
    const columns = this.newColumns;
    this.newColumns = [];
    const rows = this.newRows;
    this.newRows = [];
    const cells = this.newCells;
    this.newCells = [];
    await Promise.all([
      this.columnRepository.insert(columns),
      this.rowRepository.insert(rows),
    ]);
    await this.cellRepository.insert(cells);
  }

  private addCell(row: Row, name: string, value: string) {
    const cell = new Cell();
    cell.row = row;
    cell.column = this.getColumn(name);
    cell.value = value;
    this.newCells.push(cell);
  }

  private getColumn(name: string) {
    if (!this.columns.has(name)) {
      const column = new Column();
      column.dataset = this.dataset;
      column.header = name;
      column.columnIndex = this.colIndex++;
      this.newColumns.push(column);
      this.columns.set(name, column);
    }
    return this.columns.get(name) as Column;
  }
}
