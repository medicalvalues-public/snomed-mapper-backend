import { Test, TestingModule } from '@nestjs/testing';
import { createReadStream, ReadStream } from 'fs';
import { Readable } from 'stream';
import { DataSource } from 'typeorm';
import { Dataset } from '../../data/entity/dataset';
import { CsvImportHandler } from './csv-import-handler';
import { CsvImportService } from './csv-import.service';

jest.mock('./csv-import-handler');
jest.mock('fs');

describe('CsvImportService', () => {
  let service: CsvImportService;

  beforeEach(async () => {
    jest.resetAllMocks();
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CsvImportService,
        {
          provide: DataSource,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<CsvImportService>(CsvImportService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create an import handler', async () => {
    const dataset = new Dataset();
    dataset.delimiter = ',';
    dataset.headerIncluded = true;

    jest
      .mocked(createReadStream)
      .mockReturnValue(
        Readable.from(['test,string\n', 'val1,val2']) as ReadStream,
      );
    await service.importFile({ path: 'testPath', dataset });

    expect(jest.mocked(CsvImportHandler)).toHaveBeenCalled();
  });

  it('should send rows to the import handler', async () => {
    const dataset = new Dataset();
    dataset.delimiter = ',';
    dataset.headerIncluded = true;

    jest
      .mocked(createReadStream)
      .mockReturnValue(
        Readable.from(['test,string\n', 'val1,val2']) as ReadStream,
      );
    await service.importFile({ path: 'testPath', dataset });

    const importHandler = jest.mocked(CsvImportHandler).mock.instances[0];

    expect(importHandler.addRow as jest.Func).toHaveBeenCalledWith({
      test: 'val1',
      string: 'val2',
    });
    expect(importHandler.addRow as jest.Func).toHaveBeenCalledTimes(1);
  });

  it('should finalize once all rows have been processed', async () => {
    const dataset = new Dataset();
    dataset.delimiter = ',';
    dataset.headerIncluded = true;

    jest
      .mocked(createReadStream)
      .mockReturnValue(
        Readable.from(['test,string\n', 'val1,val2']) as ReadStream,
      );
    await service.importFile({ path: 'testPath', dataset });

    const importHandler = jest.mocked(CsvImportHandler).mock.instances[0];

    expect(importHandler.finalize as jest.Func).toHaveBeenCalled();
  });
});
