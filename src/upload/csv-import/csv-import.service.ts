import { Injectable } from '@nestjs/common';
import { createReadStream, PathLike } from 'fs';
import { DataSource } from 'typeorm';
import { Dataset } from '../../data/entity/dataset';
import { parse } from 'csv-parse';
import { CsvImportHandler } from './csv-import-handler';

@Injectable()
export class CsvImportService {
  constructor(private dataSource: DataSource) {}

  public async importFile({
    path,
    dataset,
  }: {
    path: PathLike;
    dataset: Dataset;
  }): Promise<Dataset> {
    const importHandler = new CsvImportHandler(dataset, this.dataSource);

    const stream = createReadStream(path).pipe(
      parse({
        delimiter: dataset.delimiter,
        columns: dataset.headerIncluded,
      }),
    );

    for await (const rowData of stream) {
      importHandler.addRow(rowData);
    }

    return importHandler.finalize();
  }
}
