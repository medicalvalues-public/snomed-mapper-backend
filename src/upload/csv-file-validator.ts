import { FileValidator } from '@nestjs/common/pipes/file/file-validator.interface';

export class CsvFileValidator extends FileValidator {
  private csvMimeTypes: string[] = [
    'text/x-csv',
    'application/vnd.ms-excel',
    'application/csv',
    'application/x-csv',
    'text/csv',
    'text/comma-separated-values',
    'text/x-comma-separated-values',
    'text/tab-separated-values',
  ];

  constructor() {
    super({});
  }

  buildErrorMessage(file: Express.Multer.File): string {
    const mimeType = this.getMimeType(file);
    return 'forbidden mime type ' + mimeType;
  }

  isValid(file: Express.Multer.File): boolean {
    return this.csvMimeTypes.includes(this.getMimeType(file));
  }

  private getMimeType(file: Express.Multer.File): string {
    return file.mimetype;
  }
}
