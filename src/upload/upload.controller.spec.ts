import { Test, TestingModule } from '@nestjs/testing';
import { CsvImportService } from './csv-import/csv-import.service';
import { UploadController } from './upload.controller';

jest.mock('./csv-import/csv-import.service');

describe('UploadController', () => {
  let controller: UploadController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UploadController],
      providers: [
        { provide: CsvImportService, useValue: jest.mocked(CsvImportService) },
      ],
    }).compile();

    controller = module.get<UploadController>(UploadController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
