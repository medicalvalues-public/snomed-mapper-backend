// eslint-disable-next-line max-classes-per-file
import {
  Body,
  Controller,
  MaxFileSizeValidator,
  ParseFilePipe,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { unlink } from 'fs/promises';
import { Dataset } from '../data/entity/dataset';
import { CsvImportService } from './csv-import/csv-import.service';
import {
  IsBoolean,
  IsIn,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Transform } from 'class-transformer';
import { SemanticTags } from '../snowstorm-adapter/semanticTags';
import { CsvFileValidator } from './csv-file-validator';

class CreateDatasetBody {
  @IsString()
  fileName: string;

  @IsString()
  @MaxLength(1)
  @MinLength(1)
  delimiter: string;

  @IsBoolean()
  @Transform((data) => data.value?.toLowerCase() === 'true')
  headerIncluded: boolean;

  @IsOptional()
  @IsIn(SemanticTags)
  semanticTag: string | null = null;

  @IsIn(['de', 'en'])
  language: string;
}

@Controller('upload')
export class UploadController {
  constructor(private csvImport: CsvImportService) {}

  @Post('csv')
  @UseInterceptors(FileInterceptor('file'))
  async uploadCsv(
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({ maxSize: 1024 * 1024 * 100 /* 100 MB */ }),
          new CsvFileValidator(),
        ],
      }),
    )
    file: Express.Multer.File,
    @Body() params: CreateDatasetBody,
  ) {
    try {
      const dataset = new Dataset();

      dataset.fileName = params.fileName;
      dataset.delimiter = params.delimiter;
      dataset.headerIncluded = params.headerIncluded;
      dataset.language = params.language;
      dataset.semanticTag = params.semanticTag;

      dataset.rowCount = 0;
      await this.csvImport.importFile({
        dataset,
        path: file.path,
      });

      return dataset;
    } finally {
      await unlink(file.path);
    }
  }
}
