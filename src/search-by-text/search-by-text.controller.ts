import { Controller, Get, Query } from '@nestjs/common';
import {
  Snomed,
  SnowstormAdapterService,
} from '../snowstorm-adapter/snowstorm-adapter.service';
import { Observable } from 'rxjs';
import { IsIn, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { SemanticTags } from '../snowstorm-adapter/semanticTags';

class SearchByTextQuery {
  @IsNotEmpty()
  @IsString()
  query: string;

  @IsOptional()
  @IsIn(SemanticTags)
  semanticTag: string | null = null;
}

@Controller('search-by-text/')
export class SearchByTextController {
  constructor(private snowStormAdapter: SnowstormAdapterService) {}

  @Get()
  searchByText(
    @Query() { query, semanticTag }: SearchByTextQuery,
  ): Observable<Snomed[]> {
    return this.snowStormAdapter.getSuggestionsDescriptionApi(
      query,
      semanticTag,
    );
  }
}
