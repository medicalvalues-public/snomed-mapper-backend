import { Module } from '@nestjs/common';
import { SnowstormAdapterModule } from '../snowstorm-adapter/snowstorm-adapter.module';
import { SearchByTextController } from './search-by-text.controller';

@Module({
  controllers: [SearchByTextController],
  imports: [SnowstormAdapterModule],
})
export class SearchByTextModule {}
