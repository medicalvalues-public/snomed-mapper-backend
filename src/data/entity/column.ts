import {
  BaseEntity,
  Column as OrmColumn,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Cell } from './cell';
import { Dataset } from './dataset';

@Entity({
  orderBy: {
    columnIndex: 'ASC',
  },
})
export class Column extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Dataset, (dataset) => dataset.columns, {
    nullable: false,
    eager: true,
    onDelete: 'CASCADE',
  })
  @Index()
  dataset: Dataset;

  @OrmColumn()
  columnIndex: number;

  @OrmColumn({ nullable: true })
  header?: string;

  @OrmColumn({ nullable: true, type: 'boolean' })
  containsDescription: boolean | null;

  @OneToMany(() => Cell, (cell) => cell.column)
  cells: Promise<Cell[]>;
}
