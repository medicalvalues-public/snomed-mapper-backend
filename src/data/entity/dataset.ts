import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Row } from './row';
import { Column as ColumnEntity } from './column';

export enum SnomedMappingState {
  UPLOADED = 'uploaded',
  RELEVANT_COLUMNS_DEFINED = 'relevant_columns_defined',
  FINISHED = 'finished',
}

@Entity()
export class Dataset extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  language: string;

  @Column()
  headerIncluded: boolean;

  @Column()
  fileName: string;

  @Column()
  delimiter: string;

  @Column()
  rowCount: number;

  @Column()
  snomedMappingState: SnomedMappingState = SnomedMappingState.UPLOADED;

  @Column({ type: 'varchar', nullable: true })
  semanticTag: string | null;

  @OneToMany(() => Row, (row) => row.dataset, { cascade: true })
  rows: Promise<Row[]>;

  @OneToMany(() => ColumnEntity, (column) => column.dataset, {
    cascade: true,
  })
  columns: Promise<ColumnEntity[]>;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
