import {
  BaseEntity,
  Column,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Row } from './row';
import { Column as ColumnEntity } from './column';

@Entity()
export class Cell extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  value: string;

  @ManyToOne(() => Row, (row) => row.cells, {
    nullable: false,
    eager: true,
    onDelete: 'CASCADE',
  })
  @Index()
  row: Row;

  @ManyToOne(() => ColumnEntity, (column) => column.cells, {
    nullable: false,
    eager: true,
    onDelete: 'CASCADE',
  })
  @Index()
  column: ColumnEntity;
}
