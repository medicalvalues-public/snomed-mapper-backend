import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Row } from './row';

@Entity({
  orderBy: {
    index: 'ASC',
  },
})
export class SnomedSuggestion extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  index: number;

  @Column({ nullable: false })
  snomed: string;

  @Column({ nullable: false })
  description: string;

  @ManyToOne(() => Row, (row) => row.suggestedSnomeds, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  row: Row;
}
