import {
  BaseEntity,
  Column,
  Entity,
  Index,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Cell } from './cell';
import { Dataset } from './dataset';
import { SnomedSuggestion } from './snomedSuggestion';
import { Snomed } from '../../snowstorm-adapter/snowstorm-adapter.service';

export enum RowStatus {
  UNSET = 'unset',
  SUGGESTION = 'suggested',
  MAPPED = 'mapped',
  SKIPPED = 'skipped',
}

@Entity({
  orderBy: {
    rowIndex: 'ASC',
  },
})
export class Row extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Dataset, (dataset) => dataset.rows, {
    nullable: false,
    eager: true,
    onDelete: 'CASCADE',
  })
  @Index()
  dataset: Dataset;

  @Column()
  rowIndex: number;

  @OneToMany(() => Cell, (cell) => cell.row)
  cells: Promise<Cell[]>;

  @Column({ nullable: true })
  snomed?: string;

  @Column({ nullable: true })
  snomedDescription?: string;

  @Column({ nullable: true })
  rowStatus: RowStatus = RowStatus.UNSET;

  @Column({ nullable: true, default: false })
  suggestionsLoaded: boolean;

  @OneToMany(() => SnomedSuggestion, (suggestion) => suggestion.row)
  suggestedSnomeds: Promise<SnomedSuggestion[]>;

  async sortedCells(): Promise<Cell[]> {
    return (await this.cells).sort(
      (a, b) => a.column.columnIndex - b.column.columnIndex,
    );
  }

  async values(): Promise<string[]> {
    return (await this.sortedCells()).map((cell) => cell.value);
  }

  async suggestions(): Promise<Snomed[]> {
    return (await this.suggestedSnomeds)
      .sort((a, b) => a.index - b.index)
      .map((snomed) => ({
        snomed: snomed.snomed,
        description: snomed.description,
      }));
  }
}
