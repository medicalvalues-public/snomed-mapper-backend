import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddSemanticTagToDatset1666952098658 implements MigrationInterface {
  name = 'AddSemanticTagToDatset1666952098658';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "dataset" ADD "semanticTag" character varying`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "dataset" DROP COLUMN "semanticTag"`);
  }
}
