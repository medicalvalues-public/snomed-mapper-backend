import { MigrationInterface, QueryRunner } from 'typeorm';

export class initial1665058270830 implements MigrationInterface {
  name = 'initial1665058270830';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "column" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "columnIndex" integer NOT NULL, "header" character varying, "datasetId" uuid NOT NULL, CONSTRAINT "PK_cee3c7ee3135537fb8f5df4422b" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_b973e8d0653537132b73a2efea" ON "column" ("datasetId") `,
    );
    await queryRunner.query(
      `CREATE TABLE "dataset" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "language" character varying NOT NULL, "headerIncluded" boolean NOT NULL, "fileName" character varying NOT NULL, "delimiter" character varying NOT NULL, "loincMappingState" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_36c1c67adb3d1dd69ae57f18913" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "row" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "rowIndex" integer NOT NULL, "datasetId" uuid NOT NULL, CONSTRAINT "PK_8a1504a78acbc2e1273f69f03aa" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_01a9e26246ed69cd73aa2dd736" ON "row" ("datasetId") `,
    );
    await queryRunner.query(
      `CREATE TABLE "cell" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "value" character varying NOT NULL, "snomed" character varying, "rowId" uuid NOT NULL, "columnId" uuid NOT NULL, CONSTRAINT "PK_6f34717c251843e5ca32fc1b2b8" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_b733b2095ed59cc38c446b681e" ON "cell" ("rowId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_d4ba711fa2e6ea02c89ed1cf4c" ON "cell" ("columnId") `,
    );
    await queryRunner.query(
      `ALTER TABLE "column" ADD CONSTRAINT "FK_b973e8d0653537132b73a2efeaf" FOREIGN KEY ("datasetId") REFERENCES "dataset"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "row" ADD CONSTRAINT "FK_01a9e26246ed69cd73aa2dd7369" FOREIGN KEY ("datasetId") REFERENCES "dataset"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "cell" ADD CONSTRAINT "FK_b733b2095ed59cc38c446b681e3" FOREIGN KEY ("rowId") REFERENCES "row"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "cell" ADD CONSTRAINT "FK_d4ba711fa2e6ea02c89ed1cf4cd" FOREIGN KEY ("columnId") REFERENCES "column"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "cell" DROP CONSTRAINT "FK_d4ba711fa2e6ea02c89ed1cf4cd"`,
    );
    await queryRunner.query(
      `ALTER TABLE "cell" DROP CONSTRAINT "FK_b733b2095ed59cc38c446b681e3"`,
    );
    await queryRunner.query(
      `ALTER TABLE "row" DROP CONSTRAINT "FK_01a9e26246ed69cd73aa2dd7369"`,
    );
    await queryRunner.query(
      `ALTER TABLE "column" DROP CONSTRAINT "FK_b973e8d0653537132b73a2efeaf"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_d4ba711fa2e6ea02c89ed1cf4c"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_b733b2095ed59cc38c446b681e"`,
    );
    await queryRunner.query(`DROP TABLE "cell"`);
    await queryRunner.query(
      `DROP INDEX "public"."IDX_01a9e26246ed69cd73aa2dd736"`,
    );
    await queryRunner.query(`DROP TABLE "row"`);
    await queryRunner.query(`DROP TABLE "dataset"`);
    await queryRunner.query(
      `DROP INDEX "public"."IDX_b973e8d0653537132b73a2efea"`,
    );
    await queryRunner.query(`DROP TABLE "column"`);
  }
}
