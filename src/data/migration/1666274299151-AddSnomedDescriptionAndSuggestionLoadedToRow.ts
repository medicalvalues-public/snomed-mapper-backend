import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddSnomedDescriptionAndSuggestionLoadedToRow1666274299151
  implements MigrationInterface
{
  name = 'AddSnomedDescriptionAndSuggestionLoadedToRow1666274299151';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "row" ADD "snomedDescription" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "row" ADD "suggestionsLoaded" boolean DEFAULT false`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "row" DROP COLUMN "suggestionsLoaded"`,
    );
    await queryRunner.query(
      `ALTER TABLE "row" DROP COLUMN "snomedDescription"`,
    );
  }
}
