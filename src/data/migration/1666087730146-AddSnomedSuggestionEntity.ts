import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddSnomedSuggestionEntity1666087730146
  implements MigrationInterface
{
  name = 'AddSnomedSuggestionEntity1666087730146';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "snomed_suggestion" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "index" integer NOT NULL, "snomed" character varying NOT NULL, "description" character varying NOT NULL, "rowId" uuid NOT NULL, CONSTRAINT "PK_a1f81c8b39388190dfd2818c8f7" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "snomed_suggestion" ADD CONSTRAINT "FK_79e3c3d231ccae6e78a7028dba7" FOREIGN KEY ("rowId") REFERENCES "row"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "snomed_suggestion" DROP CONSTRAINT "FK_79e3c3d231ccae6e78a7028dba7"`,
    );
    await queryRunner.query(`DROP TABLE "snomed_suggestion"`);
  }
}
