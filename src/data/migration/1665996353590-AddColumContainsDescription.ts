import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddColumContainsDescription1665996353590
  implements MigrationInterface
{
  name = 'AddColumContainsDescription1665996353590';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "dataset" RENAME COLUMN "loincMappingState" TO "snomedMappingState"`,
    );
    await queryRunner.query(`ALTER TABLE "cell" DROP COLUMN "snomed"`);
    await queryRunner.query(`ALTER TABLE "row" ADD "snomed" character varying`);
    await queryRunner.query(
      `ALTER TABLE "row" ADD "rowStatus" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "column" ADD "containsDescription" boolean`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "column" DROP COLUMN "containsDescription"`,
    );
    await queryRunner.query(`ALTER TABLE "row" DROP COLUMN "rowStatus"`);
    await queryRunner.query(`ALTER TABLE "row" DROP COLUMN "snomed"`);
    await queryRunner.query(
      `ALTER TABLE "cell" ADD "snomed" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "dataset" RENAME COLUMN "snomedMappingState" TO "loincMappingState"`,
    );
  }
}
