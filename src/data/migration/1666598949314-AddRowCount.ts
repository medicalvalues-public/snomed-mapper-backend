import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddRowCount1666598949314 implements MigrationInterface {
  name = 'AddRowCount1666598949314';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE "dataset" ADD "rowCount" integer;
            UPDATE dataset d
                SET "rowCount" = (select count(*) from "row" where "row"."datasetId"=d.id);
            ALTER TABLE "dataset" ALTER COLUMN "rowCount" SET NOT NULL;`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "dataset" DROP COLUMN "rowCount"`);
  }
}
