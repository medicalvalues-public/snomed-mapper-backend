import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';
import { Dataset, SnomedMappingState } from '../data/entity/dataset';
import { Row, RowStatus } from '../data/entity/row';
import { Column } from '../data/entity/column';
import { Snomed } from '../snowstorm-adapter/snowstorm-adapter.service';

export interface TableData {
  filename: string;
  tableId: string;
  headersIncluded: boolean;
  language: string;
  rowCount: number;
  semanticTag: string | null;
  columns: Array<{
    header: string | undefined;
    containsDescription: boolean;
  }>;
  rows: Array<{
    rowIndex: number;
    values: string[];
    status: RowStatus;
    snomed: string | undefined;
    snomedDescription: string | undefined;
    suggestions?: Snomed[];
  }>;
}

@Injectable()
export class DatasetService {
  private datasetRepository = this.dataSource.getRepository(Dataset);
  private rowRepository = this.dataSource.getRepository(Row);

  constructor(private dataSource: DataSource) {}

  public getDataset(
    datasetId: string,
    preloadTable: boolean,
  ): Promise<Dataset | null> {
    return this.datasetRepository.findOne({
      where: { id: datasetId },
      relations: preloadTable
        ? {
            rows: { cells: true },
            columns: true,
          }
        : {},
      order: preloadTable
        ? {
            columns: { columnIndex: 'ASC' },
            rows: { rowIndex: 'ASC' },
          }
        : {},
    });
  }

  public async getTableData(
    dataset: Dataset,
    count: number,
    offset: number,
  ): Promise<TableData> {
    const columns = await dataset.columns;
    const rows = await this.getRows(dataset, count, offset);
    return this.toTableData({ dataset, columns, rows });
  }

  public async toTableData(
    data: {
      dataset: Dataset;
      columns: Column[];
      rows: Row[];
    },
    options: { includeSuggestions?: boolean } = {},
  ): Promise<TableData> {
    const { dataset, columns, rows } = data;
    const { includeSuggestions } = options;
    const rowValues: TableData['rows'] = await Promise.all(
      rows.map(async (row) => ({
        rowIndex: row.rowIndex,
        values: await row.values(),
        snomed: row.snomed ?? undefined,
        snomedDescription: row.snomedDescription ?? undefined,
        status: row.rowStatus,
        suggestions: includeSuggestions ? await row.suggestions() : undefined,
      })),
    );

    return {
      filename: dataset.fileName,
      tableId: dataset.id,
      headersIncluded: dataset.headerIncluded,
      language: dataset.language,
      rowCount: dataset.rowCount,
      semanticTag: dataset.semanticTag,
      columns: columns.map((col) => ({
        header: col.header,
        containsDescription: !!col.containsDescription,
      })),
      rows: rowValues,
    };
  }

  public async updateDatasetStatus(
    dataset: Dataset,
    status: SnomedMappingState,
  ): Promise<void> {
    dataset.snomedMappingState = status;
    await dataset.save();
  }

  public async setRelevantColumns(
    dataset: Dataset,
    relevantColumnIndices: number[],
  ) {
    const columns = await dataset.columns;
    await Promise.all(
      columns.map((column) => {
        column.containsDescription = relevantColumnIndices.includes(
          column.columnIndex,
        );
        return column.save();
      }),
    );
  }

  public getRows(
    dataset: Dataset,
    count: number,
    offset: number,
  ): Promise<Row[]> {
    return this.dataSource.getRepository(Row).find({
      relations: {
        cells: true,
        suggestedSnomeds: true,
      },
      where: { dataset: { id: dataset.id } },
      take: count,
      skip: offset,
      order: { rowIndex: 'ASC' },
    });
  }

  public async updateRow(
    datasetId: string,
    rowIndex: number,
    patch: { snomed: string; snomedDescription: string; rowStatus: RowStatus },
  ) {
    return this.rowRepository.update(
      {
        dataset: { id: datasetId },
        rowIndex,
      },
      patch,
    );
  }
}
