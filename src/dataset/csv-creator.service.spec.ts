import { Dataset } from '../data/entity/dataset';
import { Row, RowStatus } from '../data/entity/row';
import { Cell } from '../data/entity/cell';
import { Column } from '../data/entity/column';
import { CsvCreatorService } from './csv-creator.service';

describe('createCsvServcie', () => {
  let dataset: Dataset;
  let rows: Row[];
  const csvService = new CsvCreatorService();

  beforeEach(() => {
    const values: string[][] = [
      ['a', 'b'],
      ['c', 'd'],
    ];
    const headers: string[] = ['col1', 'col2'];
    dataset = new Dataset();

    const columns = headers.map((header, index) => {
      const column = new Column();
      column.header = header;
      column.columnIndex = index;
      return column;
    });
    dataset.columns = Promise.resolve(columns);

    rows = values.map((rowValues, index) => {
      const row = new Row();
      row.rowIndex = index;
      row.cells = Promise.resolve(
        rowValues.map((value, colIndex) => {
          const cell = new Cell();
          cell.value = value;
          cell.column = columns[colIndex];
          return cell;
        }),
      );
      return row;
    });
    dataset.rows = Promise.resolve(rows);

    dataset.delimiter = ';';
    dataset.fileName = 'myFile.csv';

    rows[0].rowStatus = RowStatus.MAPPED;
    rows[0].snomed = '123';
    rows[0].snomedDescription = 'description123';
    rows[1].rowStatus = RowStatus.UNSET;
  });

  it('returns filename with added .mapped', async () => {
    const { fileName } = await csvService.createCsv(dataset);
    expect(fileName).toEqual('myFile.mapped.csv');
  });

  it('returns dataset without headers', async () => {
    const { csvAsString } = await csvService.createCsv(dataset);
    expect(csvAsString).toEqual('a;b;123;description123\n' + 'c;d;;\n');
  });

  it('returns dataset with headers', async () => {
    dataset.headerIncluded = true;
    const { csvAsString } = await csvService.createCsv(dataset);
    expect(csvAsString).toEqual(
      'col1;col2;SNOMED;SNOMED Description\n' +
        'a;b;123;description123\n' +
        'c;d;;\n',
    );
  });
});
