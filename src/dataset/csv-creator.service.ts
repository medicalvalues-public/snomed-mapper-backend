import { Injectable } from '@nestjs/common';
import { Dataset } from '../data/entity/dataset';
import { Row, RowStatus } from '../data/entity/row';
import { createArrayCsvStringifier } from 'csv-writer';

@Injectable()
export class CsvCreatorService {
  public async createCsv(
    dataset: Dataset,
  ): Promise<{ fileName: string; csvAsString: string }> {
    const tableContent: string[][] = [];
    if (dataset.headerIncluded) {
      tableContent.push(await this.getHeaderRow(dataset));
    }
    const rows = await dataset.rows;

    tableContent.push(
      ...(await Promise.all(rows.map((row) => this.getRowData(row)))),
    );
    const csvWriter = createArrayCsvStringifier({
      fieldDelimiter: dataset.delimiter,
    });
    return {
      fileName: this.getFileName(dataset),
      csvAsString: csvWriter.stringifyRecords(tableContent),
    };
  }

  private async getHeaderRow(datset: Dataset): Promise<string[]> {
    const columns = await datset.columns;
    return [
      ...columns.map((column) => column.header || ''),
      'SNOMED',
      'SNOMED Description',
    ];
  }

  private async getRowData(row: Row): Promise<string[]> {
    const rowData = await row.values();
    if (row.rowStatus === RowStatus.MAPPED) {
      rowData.push(row.snomed || '', row.snomedDescription || '');
    } else rowData.push('', '');
    return rowData;
  }

  private getFileName(dataset: Dataset): string {
    return dataset.fileName.replace(/\.csv$/i, '.mapped.csv');
  }
}
