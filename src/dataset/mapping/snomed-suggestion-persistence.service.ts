import { Injectable } from '@nestjs/common';
import { SnomedSuggestion } from '../../data/entity/snomedSuggestion';
import { DataSource } from 'typeorm';
import { Row, RowStatus } from '../../data/entity/row';
import { Snomed } from '../../snowstorm-adapter/snowstorm-adapter.service';

@Injectable()
export class SnomedSuggestionPersistenceService {
  private suggestionRepo = this.datasource.getRepository(SnomedSuggestion);

  constructor(private datasource: DataSource) {}

  async persistSuggestions(snomeds: Snomed[], row: Row): Promise<Row> {
    if (!snomeds.length) {
      row.suggestedSnomeds = Promise.resolve([]);
      return row;
    }
    row.rowStatus = RowStatus.SUGGESTION;
    row.snomed = snomeds[0].snomed;
    row.snomedDescription = snomeds[0].description;
    row.suggestionsLoaded = true;
    await row.save();
    const entities = Promise.all(
      snomeds.map((snomed: Snomed, i: number) => {
        const snomedSuggestion = this.suggestionRepo.create({
          snomed: snomed.snomed,
          description: snomed.description,
          index: i,
        });
        snomedSuggestion.row = row;
        return snomedSuggestion.save();
      }),
    );

    // to avoid to row.reload:
    row.suggestedSnomeds = Promise.resolve(entities);
    return row;
  }
}
