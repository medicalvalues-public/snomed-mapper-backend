import { Injectable } from '@nestjs/common';
import { Dataset } from '../../data/entity/dataset';
import { Row } from '../../data/entity/row';
import { SnowstormAdapterService } from '../../snowstorm-adapter/snowstorm-adapter.service';
import { forkJoin, from, map, Observable, of, switchMap } from 'rxjs';
import { SnomedSuggestionPersistenceService } from './snomed-suggestion-persistence.service';
import { DatasetService } from '../dataset.service';

const rowValuesToText = (rowValues: string[], indices: number[]) =>
  rowValues.filter((_value, index) => indices.includes(index)).join(' ');

@Injectable()
export class MappingService {
  constructor(
    private datasetService: DatasetService,
    private persistence: SnomedSuggestionPersistenceService,
    private snowstormAdapter: SnowstormAdapterService,
  ) {}

  public async getSuggestions(dataset: Dataset, count: number, offset: number) {
    const rows = await this.datasetService.getRows(dataset, count, offset);
    const columns = await dataset.columns;
    const relevantIndices = columns
      .filter((column) => column.containsDescription)
      .map((column) => column.columnIndex);
    return forkJoin(
      rows.map((row) =>
        this.loadSuggestionsForRow(row, relevantIndices, dataset.semanticTag),
      ),
    ).pipe(
      map((rowsWithSuggestions) =>
        this.datasetService.toTableData(
          { dataset, columns, rows: rowsWithSuggestions },
          { includeSuggestions: true },
        ),
      ),
    );
  }

  private loadSuggestionsForRow(
    row: Row,
    relevantIndices: number[],
    sematicTag: string | null,
  ): Observable<Row> {
    if (row.suggestionsLoaded) {
      return of(row);
    }
    return from(row.values()).pipe(
      map((values) => rowValuesToText(values, relevantIndices)),
      switchMap((text) =>
        this.snowstormAdapter.getSuggestionsDescriptionApi(text, sematicTag),
      ),
      switchMap((suggestions) =>
        this.persistence.persistSuggestions(suggestions, row),
      ),
    );
  }
}
