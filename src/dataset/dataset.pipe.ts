// eslint-disable-next-line max-classes-per-file
import { Injectable, NotFoundException, PipeTransform } from '@nestjs/common';
import { DatasetService } from './dataset.service';
import { Dataset } from '../data/entity/dataset';

@Injectable()
export class DatasetPipe implements PipeTransform {
  constructor(protected datasetService: DatasetService) {}

  async transform(datasetId: string): Promise<Dataset> {
    const dataset = await this.loadDataset(datasetId);
    if (!dataset) {
      throw new NotFoundException(`could not find dataset ${datasetId}`);
    }
    return dataset;
  }

  protected loadDataset(datasetId: string): Promise<Dataset | null> {
    return this.datasetService.getDataset(datasetId, false);
  }
}

@Injectable()
export class PrefilledDatasetPipe extends DatasetPipe implements PipeTransform {
  constructor(protected datasetService: DatasetService) {
    super(datasetService);
  }

  protected loadDataset(datasetId: string): Promise<Dataset | null> {
    return this.datasetService.getDataset(datasetId, true);
  }
}
