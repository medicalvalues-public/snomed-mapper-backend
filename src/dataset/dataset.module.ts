import { Module } from '@nestjs/common';

import { DatasetController } from './dataset.controller';
import { DatasetPipe, PrefilledDatasetPipe } from './dataset.pipe';
import { DatasetService } from './dataset.service';
import { MappingService } from './mapping/mapping.service';
import { SnomedSuggestionPersistenceService } from './mapping/snomed-suggestion-persistence.service';
import { SnowstormAdapterModule } from '../snowstorm-adapter/snowstorm-adapter.module';
import { CsvCreatorService } from './csv-creator.service';

@Module({
  controllers: [DatasetController],
  providers: [
    DatasetService,
    DatasetPipe,
    PrefilledDatasetPipe,

    MappingService,
    SnomedSuggestionPersistenceService,
    CsvCreatorService,
  ],
  imports: [SnowstormAdapterModule],
})
export class DatasetModule {}
