// eslint-disable-next-line max-classes-per-file
import {
  Body,
  Controller,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
  Query,
  Res,
} from '@nestjs/common';
import { DatasetPipe, PrefilledDatasetPipe } from './dataset.pipe';
import { Dataset, SnomedMappingState } from '../data/entity/dataset';
import {
  ArrayMinSize,
  IsArray,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';
import { DatasetService, TableData } from './dataset.service';
import { Type } from 'class-transformer';
import { MappingService } from './mapping/mapping.service';
import { RowStatus } from '../data/entity/row';
import { CsvCreatorService } from './csv-creator.service';

import type { Response } from 'express';

class SetRelevantColumnsBody {
  @IsArray()
  @ArrayMinSize(1)
  @IsInt({ each: true })
  relevantColumnIndices: number[];
}

class PaginationQueryParams {
  @Min(0)
  @IsInt()
  @Type(() => Number)
  @IsOptional()
  offset = 0;

  @Min(0)
  @Max(500)
  @IsInt()
  @Type(() => Number)
  @IsOptional()
  count = 50;
}

class UpdateRowBody {
  @IsInt()
  rowIndex: number;

  @IsNotEmpty()
  @IsString()
  snomed: string;

  @IsNotEmpty()
  @IsString()
  description: string;

  @IsEnum(RowStatus)
  status: RowStatus;
}

@Controller('dataset/:id')
export class DatasetController {
  constructor(
    private datasetService: DatasetService,
    private mapperService: MappingService,
    private csvCreatorService: CsvCreatorService,
  ) {}

  @Get()
  async getTable(
    @Param('id', ParseUUIDPipe, DatasetPipe) dataset: Dataset,
    @Query() query: PaginationQueryParams,
  ): Promise<TableData> {
    return this.datasetService.getTableData(dataset, query.count, query.offset);
  }

  @Post('setRelevantColumns')
  async setRelevantColumns(
    @Param('id', ParseUUIDPipe, DatasetPipe) dataset: Dataset,
    @Body() body: SetRelevantColumnsBody,
  ) {
    const { relevantColumnIndices } = body;
    await this.datasetService.setRelevantColumns(
      dataset,
      relevantColumnIndices,
    );
    await this.datasetService.updateDatasetStatus(
      dataset,
      SnomedMappingState.RELEVANT_COLUMNS_DEFINED,
    );
    return body;
  }

  @Get('suggestions')
  getSnomedSuggestions(
    @Param('id', ParseUUIDPipe, DatasetPipe) dataset: Dataset,
    @Query() query: PaginationQueryParams,
  ) {
    const { count, offset } = query;
    return this.mapperService.getSuggestions(dataset, count, offset);
  }

  @Get('mappedCsv')
  async getMappedCsv(
    @Param('id', ParseUUIDPipe, PrefilledDatasetPipe) dataset: Dataset,
    @Res() res: Response,
  ) {
    const { fileName, csvAsString } = await this.csvCreatorService.createCsv(
      dataset,
    );

    res.header('Content-Type', 'text/csv');
    res.header('access-control-expose-headers', '*');
    res.header('Content-Disposition', 'attachment; filename=' + fileName);
    res.header('filename', fileName);
    res.status(200).end(csvAsString);
  }

  @Post('updateRow')
  async updateRow(
    @Param('id', ParseUUIDPipe) datasetId: string,
    @Body() body: UpdateRowBody,
  ) {
    const { rowIndex, snomed, description, status } = body;

    await this.datasetService.updateRow(datasetId, rowIndex, {
      rowStatus: status,
      snomedDescription: description,
      snomed,
    });
  }
}
