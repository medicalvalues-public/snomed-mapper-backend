import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';

@Injectable()
export class RequestLoggerMiddleware implements NestMiddleware {
  private readonly logger = new Logger(RequestLoggerMiddleware.name);

  use(req: Request, res: Response, next: NextFunction) {
    const { ip, method, originalUrl } = req;
    const userAgent = req.get('user-agent') || '';
    const start = performance.now();
    const longRunningRequest = setTimeout(() => {
      const duration = performance.now() - start;
      this.logger.warn(
        `Long running request: ${method} ${originalUrl} ${duration.toFixed(
          2,
        )}ms`,
      );
    }, 1000);

    res.on('finish', () => {
      clearTimeout(longRunningRequest);
      const { statusCode } = res;
      const contentLength = res.get('content-length');
      const duration = performance.now() - start;

      this.logger.log(
        `${method} ${originalUrl} ${statusCode} ${contentLength}b ${duration.toFixed(
          2,
        )}ms - ${userAgent} ${ip}`,
      );
    });

    next();
  }
}
