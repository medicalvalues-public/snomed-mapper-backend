import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './config/configuration';
import { join } from 'path';
import { UploadModule } from './upload/upload.module';
import { RequestLoggerMiddleware } from './middlewares/request-logger.middleware';
import { DatasetModule } from './dataset/dataset.module';
import { SearchByTextModule } from './search-by-text/search-by-text.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory(config: ConfigService) {
        if (config.get('isTestEnvironment')) {
          return {
            type: 'sqlite',
            database: ':memory:',
            dropSchema: true,
            entities: [join(__dirname, 'data', 'entity', '**', '*')],
            synchronize: true,
          };
        } else {
          return {
            type: 'postgres',
            url: config.get<string>('database.url'),
            entities: [join(__dirname, 'data', 'entity', '**', '*')],
            migrations: [join(__dirname, 'data', 'migration', '**', '*')],
            migrationsRun: true,
            synchronize: false,
          };
        }
      },
      imports: [ConfigModule],
    }),
    UploadModule,
    DatasetModule,
    SearchByTextModule,
  ],
  controllers: [],
  providers: [RequestLoggerMiddleware],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(RequestLoggerMiddleware).forRoutes('*');
  }
}
