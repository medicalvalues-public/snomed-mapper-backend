import { DataSource } from 'typeorm';
import { Dataset } from '../../data/entity/dataset';
import { Column } from '../../data/entity/column';
import { Row } from '../../data/entity/row';
import { Cell } from '../../data/entity/cell';

export interface DatasetData {
  fileName: string;
  headers: string[];
  relevantColumnIndices: number[];
  values: string[][];
}

//creates a dataset in the database for testing purposes
export async function createDataset(
  dataSource: DataSource,
  data: DatasetData,
): Promise<Dataset> {
  const { fileName, headers, relevantColumnIndices, values } = data;
  const em = dataSource.createEntityManager();

  const dataset = await em
    .create(Dataset, {
      fileName,
      language: 'en',
      headerIncluded: true,
      delimiter: ',',
      rowCount: values.length,
    })
    .save();

  const columns: Column[] = await Promise.all(
    headers.map(async (header, index) => {
      const column: Column = em.create(Column, {
        header,
        containsDescription: relevantColumnIndices.includes(index),
        columnIndex: index,
      });
      column.dataset = dataset;
      await column.save();
      return column;
    }),
  );

  const rows: Row[] = await Promise.all(
    values.map(async (_values, index) => {
      const row: Row = em.create(Row, {
        rowIndex: index,
      });
      row.dataset = dataset;
      await row.save();
      return row;
    }),
  );

  await Promise.all(
    values.flatMap((rowValues, rowIndex) =>
      rowValues.map(async (value, colIndex) => {
        const cell: Cell = em.create(Cell, {
          value: value,
        });
        cell.row = rows[rowIndex];
        cell.column = columns[colIndex];
        await cell.save();
        return cell;
      }),
    ),
  );
  await dataset.reload();
  return dataset;
}
