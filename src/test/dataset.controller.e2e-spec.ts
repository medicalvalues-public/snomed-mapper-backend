import { INestApplication, ValidationPipe } from '@nestjs/common';
import { createTestingModule } from './e2e-testing.module';
import { DataSource, EntityManager } from 'typeorm';
import * as request from 'supertest';
import { Dataset, SnomedMappingState } from '../data/entity/dataset';
import { Column } from '../data/entity/column';
import { Row, RowStatus } from '../data/entity/row';
import { Cell } from '../data/entity/cell';
import { createDataset } from './utils/createDataset';

describe('DatasetController (e2e)', () => {
  let app: INestApplication;
  let dataSource: DataSource;
  let em: EntityManager;
  let dataset: Dataset;

  beforeEach(async () => {
    const moduleFixture = await createTestingModule().compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();
    dataSource = app.get(DataSource);
    em = dataSource.createEntityManager();
  });

  beforeEach(async () => {
    dataset = await em
      .create(Dataset, {
        language: 'en',
        fileName: 'test.csv',
        delimiter: ',',
        headerIncluded: true,
        rowCount: 0,
      })
      .save();
  });

  afterEach(() => app.close());

  describe('validate tableId', () => {
    it('returns 404 if dataset could not be found', async () => {
      await request(app.getHttpServer())
        .get('/dataset/2f225543-fde1-48c1-b677-e56a2df11e24')
        .expect(404);
    });

    it('returns 400 if tableId is not a uuid', async () => {
      await request(app.getHttpServer()).get('/dataset/foobar').expect(400);
    });
  });

  describe('/dataset/:tableId', () => {
    beforeEach(async () => {
      const [column1, column0] = await Promise.all(
        [1, 0].map(async (columnIndex) => {
          const column = em.create(Column, {
            columnIndex,
            header: 'testHeader_' + columnIndex,
            containsDescription: columnIndex === 1,
          });
          column.dataset = dataset;
          await column.save();
          return column;
        }),
      );
      await Promise.all(
        [2, 1, 0].map(async (rowIndex) => {
          const row = em.create(Row, {
            rowIndex,
          });
          row.dataset = dataset;
          await row.save();
          await Promise.all(
            [column1, column0].map(async (column) => {
              const cell = em.create(Cell, {
                value: `row ${rowIndex}, column ${column.columnIndex}`,
              });
              cell.row = row;
              cell.column = column;
              await cell.save();
            }),
          );
        }),
      );
    });

    it('returns data', async () => {
      const result = await request(app.getHttpServer()).get(
        `/dataset/${dataset.id}?count=2`,
      );

      expect(result.body).toEqual({
        filename: 'test.csv',
        headersIncluded: dataset.headerIncluded,
        language: dataset.language,
        rowCount: 0,
        tableId: dataset.id,
        semanticTag: null,
        columns: [
          { header: 'testHeader_0', containsDescription: false },
          { header: 'testHeader_1', containsDescription: true },
        ],
        rows: [
          {
            values: ['row 0, column 0', 'row 0, column 1'],
            rowIndex: 0,
            status: RowStatus.UNSET,
          },
          {
            values: ['row 1, column 0', 'row 1, column 1'],
            rowIndex: 1,
            status: RowStatus.UNSET,
          },
        ],
      });
    });
  });

  describe('/dataset/:tableId/setRelevantColumns', () => {
    let column0: Column;
    let column1: Column;

    beforeEach(async () => {
      [column0, column1] = await Promise.all(
        [0, 1].map(async (columnIndex) => {
          const column = em.create(Column, {
            columnIndex,
            containsDescription: false,
          });
          column.dataset = dataset;
          await column.save();
          return column;
        }),
      );
    });

    it('sets relevant columns', async () => {
      await request(app.getHttpServer())
        .post(`/dataset/${dataset.id}/setRelevantColumns`)
        .send({ relevantColumnIndices: [1] });
      await column0.reload();
      await column1.reload();
      expect(column0.containsDescription).toBe(false);
      expect(column1.containsDescription).toBe(true);
    });

    it('sets column back to containsDescription:false if not contained in relevantColumnArray', async () => {
      column0.containsDescription = true;
      await column0.save();
      await request(app.getHttpServer())
        .post(`/dataset/${dataset.id}/setRelevantColumns`)
        .send({ relevantColumnIndices: [1] });
      await column0.reload();
      expect(column0.containsDescription).toBe(false);
    });

    it('sets mappingStatus to RELEVANT_COLUMNS_DEFINED', async () => {
      await request(app.getHttpServer())
        .post(`/dataset/${dataset.id}/setRelevantColumns`)
        .send({ relevantColumnIndices: [1] });
      await dataset.reload();
      expect(dataset.snomedMappingState).toEqual(
        SnomedMappingState.RELEVANT_COLUMNS_DEFINED,
      );
    });
  });

  describe('update row status', () => {
    let dataset: Dataset;

    beforeEach(async () => {
      dataset = await createDataset(dataSource, {
        fileName: 'myDataset',
        headers: ['col1', 'col2'],
        relevantColumnIndices: [0],
        values: [
          ['a', 'b'],
          ['c', 'd'],
        ],
      });
    });

    it('updates row', async () => {
      const body = {
        rowIndex: 1,
        snomed: 'mySnomed',
        description: 'mySnomedDescription',
        status: RowStatus.MAPPED,
      };
      await request(app.getHttpServer())
        .post(`/dataset/${dataset.id}/updateRow`)
        .send(body)
        .expect(201);
      await dataset.reload();
      const updatedRow = (await dataset.rows)[1];

      expect(updatedRow).toEqual(
        expect.objectContaining({
          snomed: 'mySnomed',
        }),
      );
    });
  });
});
