import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { AppModule } from '../app.module';
import { SnowstormAdapterService } from '../snowstorm-adapter/snowstorm-adapter.service';
import { SnowstormAdapterServiceMock } from './mocks/snowstorm-adapter.service.mock';

@Module({
  imports: [AppModule],
})
export class E2eTestingModule {}

export function createTestingModule() {
  return Test.createTestingModule({
    imports: [E2eTestingModule],
  })
    .overrideProvider(ConfigService)
    .useValue(new ConfigService({ isTestEnvironment: true }))
    .overrideProvider(SnowstormAdapterService)
    .useClass(SnowstormAdapterServiceMock);
}
