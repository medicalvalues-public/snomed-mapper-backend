import { INestApplication, ValidationPipe } from '@nestjs/common';
import { createTestingModule } from './e2e-testing.module';
import {
  Snomed,
  SnowstormAdapterService,
} from '../snowstorm-adapter/snowstorm-adapter.service';
import * as request from 'supertest';
import { of } from 'rxjs';
import SpyInstance = jest.SpyInstance;

describe('get snomed suggestions (e2e)', () => {
  let app: INestApplication;

  let spyOnSnowstorm: SpyInstance;

  beforeEach(async () => {
    const moduleFixture = await createTestingModule().compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();

    spyOnSnowstorm = jest.spyOn(
      app.get(SnowstormAdapterService),
      'getSuggestionsDescriptionApi',
    );
  });

  describe('search-by-text-endpoint', () => {
    let mockSuggestions: Snomed[];

    beforeEach(() => {
      mockSuggestions = [
        { snomed: '123', description: 'foo' },
        { snomed: '321', description: 'bar' },
      ];
      spyOnSnowstorm.mockReturnValue(of(mockSuggestions));
    });

    it('search-by-text-endpoint returns suggestions from Snowstorm', async () => {
      const searchQuery = 'foobar';
      const result = await request(app.getHttpServer()).get(
        `/search-by-text?query=${searchQuery}`,
      );

      expect(spyOnSnowstorm).toHaveBeenCalledWith(searchQuery, null);
      expect(result.body).toEqual(mockSuggestions);
    });

    it('uses semanticTag if provided', async () => {
      const searchQuery = 'foobar';
      const semanticTag = 'finding';
      await request(app.getHttpServer()).get(
        `/search-by-text?query=${searchQuery}&semanticTag=${semanticTag}`,
      );

      expect(spyOnSnowstorm).toHaveBeenCalledWith(searchQuery, semanticTag);
    });
  });
});
