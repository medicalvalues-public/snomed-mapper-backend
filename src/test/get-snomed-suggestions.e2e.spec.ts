import { INestApplication, ValidationPipe } from '@nestjs/common';
import { createTestingModule } from './e2e-testing.module';
import { createDataset } from './utils/createDataset';
import { DataSource } from 'typeorm';
import { Dataset } from '../data/entity/dataset';
import * as request from 'supertest';
import { TableData } from '../dataset/dataset.service';
import { Row, RowStatus } from '../data/entity/row';
import { SnowstormAdapterService } from '../snowstorm-adapter/snowstorm-adapter.service';
import { of } from 'rxjs';
import SpyInstance = jest.SpyInstance;

describe('get snomed suggestions (e2e)', () => {
  let app: INestApplication;
  let dataSource: DataSource;

  let dataset: Dataset;

  let spyOnSnowstorm: SpyInstance;

  beforeEach(async () => {
    const moduleFixture = await createTestingModule().compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();

    dataSource = app.get(DataSource);
  });

  beforeEach(async () => {
    spyOnSnowstorm = jest.spyOn(
      app.get(SnowstormAdapterService),
      'getSuggestionsDescriptionApi',
    );

    dataset = await createDataset(dataSource, {
      fileName: 'test',
      headers: ['col0', 'col1', 'col2'],
      relevantColumnIndices: [1, 2],
      values: [
        ['r0c0', 'r0c1', 'r0c2'],
        ['r1c0', 'r1c1', 'r1c2'],
        ['r2c0', 'r2c1', 'r2c2'],
      ],
    });
  });

  afterEach(() => app.close());

  describe('suggestions are returned in body', () => {
    let result: TableData;
    beforeEach(async () => {
      const requestResult = await request(app.getHttpServer()).get(
        `/dataset/${dataset.id}/suggestions?count=1&offset=1`,
      );
      result = requestResult.body;
    });

    it('returns data paginated', () => {
      expect(result.rows.length).toEqual(1);
      expect(result.rows[0].rowIndex).toEqual(1);
    });

    it('returns suggestions for Row', () => {
      const firstRow = result.rows[0];
      expect(firstRow.suggestions).toEqual([
        {
          snomed: 'sug:0:r1c1 r1c2',
          description: 'suggestion 0 for r1c1 r1c2',
        },
        {
          snomed: 'sug:1:r1c1 r1c2',
          description: 'suggestion 1 for r1c1 r1c2',
        },
        {
          snomed: 'sug:2:r1c1 r1c2',
          description: 'suggestion 2 for r1c1 r1c2',
        },
      ]);
    });

    it('sets first suggestion as snowmed on row', () => {
      const firstRow = result.rows[0];
      expect(firstRow.snomed).toEqual('sug:0:r1c1 r1c2');
      expect(firstRow.snomedDescription).toEqual('suggestion 0 for r1c1 r1c2');
    });

    it('sets first suggestion as snowmed on row', () => {
      const firstRow = result.rows[0];
      expect(firstRow.status).toEqual(RowStatus.SUGGESTION);
    });
  });

  describe('suggestions are persisted', () => {
    let rowWithSuggestions: Row;
    beforeEach(async () => {
      await request(app.getHttpServer()).get(
        `/dataset/${dataset.id}/suggestions?count=1&offset=1`,
      );
      await dataset.reload();
      rowWithSuggestions = (await dataset.rows)[1];
    });

    it('persists suggestions', async () => {
      const suggestions = await rowWithSuggestions.suggestions();
      expect(suggestions).toEqual([
        {
          snomed: 'sug:0:r1c1 r1c2',
          description: 'suggestion 0 for r1c1 r1c2',
        },
        {
          snomed: 'sug:1:r1c1 r1c2',
          description: 'suggestion 1 for r1c1 r1c2',
        },
        {
          snomed: 'sug:2:r1c1 r1c2',
          description: 'suggestion 2 for r1c1 r1c2',
        },
      ]);
    });

    it('sets first snomed and description to Row', () => {
      expect(rowWithSuggestions.snomed).toEqual('sug:0:r1c1 r1c2');
      expect(rowWithSuggestions.snomedDescription).toEqual(
        'suggestion 0 for r1c1 r1c2',
      );
    });

    it('sets status to SUGGESTED', () => {
      expect(rowWithSuggestions.rowStatus).toEqual(RowStatus.SUGGESTION);
      expect(rowWithSuggestions.suggestionsLoaded).toEqual(true);
    });
  });

  describe('suggestions endpoint calls SnowstormAdapter', () => {
    beforeEach(async () => {
      await request(app.getHttpServer()).get(
        `/dataset/${dataset.id}/suggestions?count=1&offset=1`,
      );
    });

    it('calls SnowStormAdapter with concatenation of relevant cell values', () => {
      expect(spyOnSnowstorm).toHaveBeenCalledWith('r1c1 r1c2', null);
    });

    it('it does not call snowStorm again if suggestions are already loaded', async () => {
      spyOnSnowstorm.mockReset();
      await request(app.getHttpServer()).get(
        `/dataset/${dataset.id}/suggestions?count=1&offset=1`,
      );
      expect(spyOnSnowstorm).not.toHaveBeenCalled();
    });
  });

  it('uses semanticTag if provided in dataset', async () => {
    dataset.semanticTag = 'disease';
    await dataset.save();
    await request(app.getHttpServer()).get(
      `/dataset/${dataset.id}/suggestions?count=1&offset=1`,
    );
    expect(spyOnSnowstorm).toHaveBeenCalledWith(expect.anything(), 'disease');
  });

  describe('works if no suggestions could be found', () => {
    let result: TableData;
    beforeEach(() => {
      spyOnSnowstorm.mockReturnValueOnce(of([]));
    });

    beforeEach(async () => {
      const requestResult = await request(app.getHttpServer()).get(
        `/dataset/${dataset.id}/suggestions?count=1&offset=1`,
      );
      result = requestResult.body;
    });

    it('returns empty set of suggestions', () => {
      const firstRow = result.rows[0];
      expect(firstRow).toEqual(
        expect.objectContaining({
          suggestions: [],
        }),
      );
      expect(firstRow.snomed).toBeUndefined();
      expect(firstRow.snomedDescription).toBeUndefined();
    });

    it('leaves suggestionsLoaded to `false`', async () => {
      await dataset.reload();
      const rows = await dataset.rows;
      rows.forEach((row) => expect(row.suggestionsLoaded).toEqual(false));
    });
  });
});
