import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { join } from 'path';
import { DataSource, Repository } from 'typeorm';
import { Dataset } from '../data/entity/dataset';
import { createTestingModule } from './e2e-testing.module';

describe('UploadController (e2e)', () => {
  let app: INestApplication;
  let datasetRepository: Repository<Dataset>;

  beforeEach(async () => {
    const moduleFixture = await createTestingModule().compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();

    const dataSource = app.get(DataSource);
    datasetRepository = dataSource.getRepository(Dataset);
  });

  afterEach(() => app.close());

  it('/upload/csv creates a dataset', async () => {
    const result = await request(app.getHttpServer())
      .post('/upload/csv')
      .attach('file', join(__dirname, 'assets', 'simple.csv'))
      .field('fileName', 'testfile')
      .field('language', 'en')
      .field('delimiter', ',')
      .field('headerIncluded', true)
      .expect(201);

    expect(
      await datasetRepository.findOneBy({ id: result.body.id }),
    ).toBeTruthy();
  });

  it('should have the csv columns after posting a file to /upload/csv', async () => {
    const result = await request(app.getHttpServer())
      .post('/upload/csv')
      .attach('file', join(__dirname, 'assets', 'simple.csv'))
      .field('fileName', 'testfile')
      .field('language', 'en')
      .field('delimiter', ',')
      .field('headerIncluded', true)
      .expect(201);

    const dataset = await datasetRepository.findOneByOrFail({
      id: result.body.id,
    });
    expect((await dataset.columns).map((column) => column.header)).toEqual([
      'col1',
      'col2',
    ]);
  });

  describe('rowCount is set correctly', () => {
    it.each`
      headersIncluded | expectedRowCount
      ${true}         | ${1}
      ${false}        | ${2}
    `(
      'sets rowCount correctly for headersIncluded $headersIncluded',
      async ({ headersIncluded, expectedRowCount }) => {
        const result = await request(app.getHttpServer())
          .post('/upload/csv')
          .attach('file', join(__dirname, 'assets', 'simple.csv'))
          .field('fileName', 'testfile')
          .field('language', 'en')
          .field('delimiter', ',')
          .field('headerIncluded', headersIncluded);
        const dataset = await datasetRepository.findOneByOrFail({
          id: result.body.id,
        });
        expect(dataset.rowCount).toBe(expectedRowCount);
      },
    );
  });
});
