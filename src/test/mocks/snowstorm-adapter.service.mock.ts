import {
  ISnowstormAdapterService,
  Snomed,
} from '../../snowstorm-adapter/snowstorm-adapter.service';
import { of } from 'rxjs';

export class SnowstormAdapterServiceMock implements ISnowstormAdapterService {
  getSuggestionsDescriptionApi(text: string) {
    const suggestions: Snomed[] = [0, 1, 2].map((i) => ({
      snomed: `sug:${i}:${text}`,
      description: `suggestion ${i} for ${text}`,
    }));
    return of(suggestions);
  }
}
