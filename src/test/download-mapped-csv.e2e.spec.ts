import { INestApplication, ValidationPipe } from '@nestjs/common';
import { createTestingModule } from './e2e-testing.module';
import { createDataset } from './utils/createDataset';
import { DataSource } from 'typeorm';
import { Dataset } from '../data/entity/dataset';
import { RowStatus } from '../data/entity/row';
import * as request from 'supertest';

describe('get snomed suggestions (e2e)', () => {
  let app: INestApplication;
  let dataSource: DataSource;

  let dataset: Dataset;

  beforeEach(async () => {
    const moduleFixture = await createTestingModule().compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();

    dataSource = app.get(DataSource);
  });

  beforeEach(async () => {
    dataset = await createDataset(dataSource, {
      fileName: 'myFile.csv',
      headers: ['col0', 'col1'],
      relevantColumnIndices: [1, 2],
      values: [['value1', 'value2']],
    });
  });

  afterEach(() => app.close());

  it('returns mapped csv', async () => {
    const row = (await dataset.rows)[0];
    row.snomed = '1234';
    row.snomedDescription = 'description1234';
    row.rowStatus = RowStatus.MAPPED;
    await row.save();
    const result = await request(app.getHttpServer()).get(
      `/dataset/${dataset.id}/mappedCsv`,
    );
    expect(result.text).toEqual(
      'col0,col1,SNOMED,SNOMED Description\n' +
        'value1,value2,1234,description1234\n',
    );
    expect(result.type).toEqual('text/csv');
  });
});
