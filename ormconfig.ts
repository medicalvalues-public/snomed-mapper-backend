import * as dotenv from 'dotenv';
import { sep } from 'path';
import { DataSource } from 'typeorm';
import config from './src/config/configuration';

dotenv.config();

const ormConfig = new DataSource({
  type: 'postgres',
  url: config().database.url,
  entities: [[__dirname, 'src', 'data', 'entity', '**', '*'].join(sep)],
  migrations: [[__dirname, 'src', 'data', 'migration', '**', '*'].join(sep)],
  synchronize: false,
  migrationsRun: true,
});
export default ormConfig;
