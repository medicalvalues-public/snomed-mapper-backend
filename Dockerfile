FROM node:18-alpine AS node

# =====
# Building Stage
FROM node AS builder
ARG NPM_REGISTRY_URL
ARG NPM_REGISTRY_TOKEN

WORKDIR /app

COPY package.json package-lock.json ./
RUN echo "@medicalvalues:registry=${NPM_REGISTRY_URL}" >> .npmrc
RUN echo "${NPM_REGISTRY_URL#https?}:_authToken=${NPM_REGISTRY_TOKEN}" >> .npmrc
RUN npm ci

COPY src tsconfig.json ./
RUN npm run build

# =====
# Application Stage
FROM node AS final

ENV SNOWSTORM_URL=""
ENV SNOWSTORM_BRANCH=MAIN

RUN apk add curl

WORKDIR /app

COPY package.json package-lock.json ./
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/dist ./dist

EXPOSE 3000

# Overwrite with --build-arg VERSION=2.2.3
ARG VERSION=unspecified
LABEL Name=snomed_mapper_backend
LABEL Version="${VERSION}"

CMD node ./dist/main.js
